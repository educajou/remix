// 2024 Arnaud Champollion, licence GNU/GPL 3.0

const divDonnees = document.getElementById('donnees');
const divRepartition = document.getElementById('repartition');
const boutonPlus = document.getElementById('bouton-plus');
const inputNombre = document.getElementById('input-nombre');
const boutonRepartir = document.getElementById('bouton-repartir');
const boutonDownload = document.getElementById('bouton-download');
const boutonCsv = document.getElementById('bouton-csv');
const caseIntitules = document.getElementById('case-intitules');
const divChoixCsv = document.getElementById('choix-csv');
const divApropos = document.getElementById('a-propos');
const divTableur = document.getElementById('tableur');
const divRgpd = document.getElementById('rgpd');
const zoneCollage = document.getElementById('zone-collage');
const imgLogo = document.getElementById('img-logo');


inputNombre.value = 2;
boutonCsv.disabled=true;
caseIntitules.checked=false;
groupesRepartis=false;

// On vérifie si des paramètres sont dans l'URL
let url = window.location.search;
let urlParams = new URLSearchParams(url);
if (urlParams.get('primtuxmenu')==="true") {
    imgLogo.style.marginLeft = '120px';
    boutonDownload.style.display='none';
 }

ajouteGroupe();

function ajouteGroupe(){
    let nouveauConteneur = document.createElement('div');
    nouveauConteneur.classList.add('groupe');

    let nouvelIntitule = document.createElement('input');
    nouvelIntitule.classList.add('zone-titre');
    nouvelIntitule.type="text";
    nouvelIntitule.placeholder='Intitulé (ex: "CM2")';

    let nouvelleZone = document.createElement('textarea');
    nouvelleZone.classList.add('zone-texte');
    nouvelleZone.placeholder='Écrire ou coller une liste\nÉlève 1\nÉlève 2\n\...';
    
    let nouveauBouton = document.createElement('button');
    nouveauBouton.classList.add('bouton-moins');
    nouveauBouton.innerText='X';
    nouveauBouton.title='Supprimer cette colonne'
    nouveauBouton.addEventListener('click',supprime);
    
    nouveauConteneur.appendChild(nouveauBouton);
    nouveauConteneur.appendChild(nouvelIntitule);
    nouveauConteneur.appendChild(nouvelleZone);
    divDonnees.insertBefore(nouveauConteneur, boutonPlus);

    let liste = [nouvelIntitule,nouvelleZone];

    return liste.slice();
}

function supprime(event){
    divAsupprimer = event.currentTarget.parentNode;
    divAsupprimer.remove();
}

function repartir(){
    divRepartition.innerHTML='';

    let intitulesBase = document.querySelectorAll('.zone-titre');
    let groupesBase = document.querySelectorAll('.zone-texte');

    let tableauBase = [];
    divGroupesRepartis = [];
    tableauRepartis = [];
    intitulesEleves = {}; // Un objet pour stocker les intitulés associés à chaque élève

    let j=0;

    groupesBase.forEach((groupe, index) => {
        let contenu = nettoie(groupe.value);
        let liste = contenu.split('\n');
        tableauBase.push(liste);

        liste.forEach(element => {
            intitulesEleves[element] = intitulesBase[index].value;
        });
    });

    nombredeGroupes = parseInt(inputNombre.value);

    for (let i = 0; i < nombredeGroupes; i++) {
        let nouveauConteneur = document.createElement('div');
        nouveauConteneur.classList.add('groupe-reparti');

        let nouvelIntitule = document.createElement('input');
        nouvelIntitule.classList.add('zone-titre','intitule-sortie');
        nouvelIntitule.type="text";

        let titreGroupe;        
        if (groupesRepartis && divIntitulesGroupesSortie[i]){
            titreGroupe = divIntitulesGroupesSortie[i].value;
        } else {
            titreGroupe = 'Groupe '+ (i+1);
        }
        nouvelIntitule.value=titreGroupe;

        let nouvelleZone = document.createElement('div');
        nouvelleZone.classList.add('zone-texte');

        divGroupesRepartis.push(nouvelleZone);
        tableauRepartis.push([]);

        nouveauConteneur.appendChild(nouvelIntitule);
        nouveauConteneur.appendChild(nouvelleZone);
        divRepartition.appendChild(nouveauConteneur);
    }

    divIntitulesGroupesSortie = document.querySelectorAll('.intitule-sortie');

    let posDansTableauReparti=0;

    tableauBase.forEach(groupe => {
    
        while (groupe.length > 0) {
            let posDansGroupe=Math.floor(Math.random() * groupe.length);
            tableauRepartis[posDansTableauReparti].push(groupe[posDansGroupe]);
            posDansTableauReparti += 1;
            if (posDansTableauReparti >= nombredeGroupes) {
                posDansTableauReparti = 0;
            }
            groupe.splice(posDansGroupe, 1);
        }   

    });

    let posGroupe = 0;
    divGroupesRepartis.forEach(groupe => {
        if (caseIntitules.checked){
            tableauRepartis[posGroupe] = tableauRepartis[posGroupe].map(eleve => {
                return intitulesEleves[eleve] + ' > ' + eleve;
            });
         }
        groupe.innerText = tableauRepartis[posGroupe].join('\n');
        posGroupe += 1;
    });

    boutonCsv.disabled=false;
    groupesRepartis=true;
}

function nettoie(saisie) {

    //Remplacer oe par œ
  saisie = saisie.replace(/oe/g, "œ");

    //Remplacer ae par æ
    saisie = saisie.replace(/ae/g, "æ");

  //Supprimer les doubles espaces
  saisie = saisie.replace(/ +/g, ' ').trim();

  //Supprimer les espaces en début et en fin de mot
  saisie=saisie.replace(/\s*(,|;|\.|\n)\s*/g, '$1');

  //Remplacer , ; par des sauts de ligne
  saisie=saisie.replace(/,|;/g, "\n");

  //Supprimer les lignes vides
  saisie = saisie.split('\n').filter(ligne => ligne.trim() !== '').join('\n');


  //Valeur de renvoi
  return saisie;

}


function transpose(tableau) {
    return tableau[0].map((col, i) => tableau.map(row => row[i]));
}

function telechargerCsv(mode) {
    divChoixCsv.classList.add('hide');
    divIntitulesGroupesSortie = document.querySelectorAll('.intitule-sortie');
    let tableauFinal = [];

    if (mode==='colonnes'){
        // Mode colonnes

        tableauFinal = transpose(tableauRepartis);        
        let intitulesGroupesSortie = [];
        for (let i = 0; i < nombredeGroupes; i++) {
            intitulesGroupesSortie.push(divIntitulesGroupesSortie[i].value)
        }
        tableauFinal.unshift(intitulesGroupesSortie);
    } else {
        // Mode lignes
        tableauFinal.push(['Groupe initial', 'Élève', 'Groupe final']);        
        tableauRepartis.forEach((groupe, index) => {
            let intituleFinal = divIntitulesGroupesSortie[index].value;
            groupe.forEach(eleve => {
                let intituleInitial = intitulesEleves[eleve];
                if (caseIntitules.checked){
                    intituleInitial=eleve.split(' > ')[0];
                    eleve=eleve.split(' > ')[1];}
                tableauFinal.push([intituleInitial, eleve, intituleFinal]);
            });
        });
    }

    // Convertir le tableau à deux niveaux en une chaîne CSV
    let csv = tableauFinal.map(row => row.join(',')).join('\n');
    
    // Créer un objet Blob
    let blob = new Blob([csv], { type: 'text/csv' });
    
    // Créer un objet URL à partir du Blob
    let url = window.URL.createObjectURL(blob);
    
    // Créer un élément d'ancre pour télécharger le fichier
    let a = document.createElement('a');
    a.href = url;
    
    // Définir le nom du fichier
    a.download = 'repartition_inversee.csv';
    
    // Simuler un clic sur l'ancre pour déclencher le téléchargement
    a.click();
    
    // Libérer l'objet URL
    window.URL.revokeObjectURL(url);
}


function intitules(){
    if (groupesRepartis){
        let posGroupe = 0;
        divGroupesRepartis.forEach(groupe => {
            if (caseIntitules.checked){
                tableauRepartis[posGroupe] = tableauRepartis[posGroupe].map(eleve => {
                    return intitulesEleves[eleve] + ' > ' + eleve;
                });
            } else {
                tableauRepartis[posGroupe] = tableauRepartis[posGroupe].map(eleve => {
                    return eleve.split(' > ')[1]; // Récupérer la partie après le dernier tiret "-"
                });
            }
            groupe.innerText = tableauRepartis[posGroupe].join('\n');
            posGroupe += 1;
        });
    }
}

function retirer(){
    if (groupesRepartis){
        repartir();
    }
}

function ouvre(div){
    div.classList.remove('hide');
    if (div===divTableur){
        zoneCollage.focus();
    }
}

function ferme(div){
    div.classList.add('hide');
}

zoneCollage.addEventListener('paste', function(event) {
    // Empêcher le collage par défaut
    event.preventDefault();

    // Récupérer le texte collé
    var clipboardData = (event.clipboardData || window.clipboardData);
    var pastedText = clipboardData.getData('text');

    // Manipuler le texte collé
    remplis(pastedText);
});

function remplis(texte){
    if (estTableau(texte)) {
        // Le texte est un tableau avec des tabulations
        console.log("Le texte collé est un tableau :");
        // Diviser le texte en lignes
        var lignes = texte.trim().split('\n');
        // Diviser chaque ligne en colonnes
        var tableau = lignes.map(function(ligne) {
            return ligne.split('\t');
        });

        let listeGroupes = document.querySelectorAll('.groupe');
        listeGroupes.forEach(groupe =>{
            groupe.remove();
        });

        tableau[0].forEach(titre => {
            listeGroupes=ajouteGroupe();
            listeGroupes[0].value=titre;
        });

        listeZones = document.querySelectorAll('.zone-texte');
        tableau.forEach((line, rowIndex) => {
            if (rowIndex != 0) {
                line.forEach((element, columnIndex) => {
                    if (rowIndex != 1) {
                        listeZones[columnIndex].value += '\n';
                    }
                    listeZones[columnIndex].value += element;
                });
            }
        });
        
        ferme(divTableur);

    } else {
        // Le texte n'est pas un tableau avec des tabulations
        zoneCollage.value='';
        zoneCollage.placeholder='Le contenu collé ne correspond pas au format attendu.'
    }
}

function estTableau(texte) {
    // Expression régulière pour rechercher des lignes séparées par des retours à la ligne
    var ligneRegex = /^(.*?\t.*?\n)+$/;
    // Vérifier si le texte correspond au schéma de tableau
    return ligneRegex.test(texte);
}
