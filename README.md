# Remix

## Principe
Remix est une application permettant de former des groupes d'élèves hétérogènes. Par exemple, à partir d'une classe de CP, de CE1 et de CE2, on peut obtenir 5 groupes contenant des élèves de chaque niveau.

Ce principe peut évidemment s'appliquer à d'autres entités que des classes et des élèves, de façon générale Remix permet de ventiler des données.

## Protection des données

Bien que l'application Remix s'éxécute dans un navigateur web, **elle ne collecte strictement aucune donnée.**
La plateforme https://educajou.forge.apps.education.fr/remix/ sur laquelle on peut l'utiliser en ligne n'a d'ailleurs pas la capacité de stocker des données en provenance de l'utilisateur. En réalité, bien qu'en ligne, l'application s'éxécute localement dans votre navigateur, sans aucun lien avec le serveur. Techniquement c'est une application "hors ligne" qui est re-téléchargée localement à chaque fois que vous l'utilisez.

Le code source est écrit intégralement en HTML, CSS et Javascript. Il est consultable [sur la page du projet](https://forge.apps.education.fr/educajou/remix).

Toutefois, vous pouvez récupérer une version hors-ligne et la placer où bon vous semble dans votre disque dur, voir ci-dessous.

## Utiliser Remix

### En ligne

Une instance de Remix est utilisable directement sur https://educajou.forge.apps.education.fr/remix/ .
Vous pouvez aussi envisager de la cloner et l'installer ailleurs, ou la télécharger pour une utilisation hors-ligne.

### Hors ligne

Pour utiliser Remix hors-ligne :
- Télécharger le projet en ZIP https://forge.apps.education.fr/educajou/remix/-/archive/main/remix-main.zip
- Extraire le ZIP (généralement, clic droit, extraire ici / tout)
- Dans le dossier nouvellement créé, ouvrir le fichier index.html

L'application s'ouvrira avec le navigateur web, bien que totalement hors ligne.


## Mode d'emploi
Pour commencer, saisssez vos groupes de départ ou importez-les depuis un tableur, puis cliquez sur "Ventiler".

Une fois les groupes ventilés, on peut obtenir une nouvelle répartition aléatoire en cliquant à nouveau sur le bouton "ventiler" (mais toujours dans les mêmes proportions).

Les noms des groupes en sortie (Groupe 1, Groupe 2 ...) sont également modifiables.

Les groupes ainsi obtenus sont exportables en CSV, qu'on peut ouvrir avec un tableur comme LibreOffice Calc ou Excel, par exemple.

## Licence
Remix est une application libre, écrite par Arnaud Champollion, et partagée sous licence GNU/GPL.

